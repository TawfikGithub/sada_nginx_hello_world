This project is to accomplish the following.

Homework Part 1: Google Kubernetes Engine

In this assignment we focus on exploring Google's managed Kubernetes offering.

Requirements
Description
Success criteria
Artifacts to be provided
Bonus points
Requirements

A Linux/OSX environment
Running windows? Spin up a virtual machine with VirtualBox. CentOS or Ubuntu are easy enough and will get the job done.
A free GCP account
Description

Acquire a Google account. Google Cloud has a free trial where they provide you with $300 in credits for a year. Use these credits to:

Spin up a Kubernetes cluster in GKE with two-three nodes
Write a Kubernetes deployment definition in YAML that includes:
an Nginx web server with a single hello-world webpage
Write a Kubernetes service definition in YAML that exposes the deployment with a Load Balancer
Write a Kubernetes ingress definition to allow the service to be reached from the world
Success criteria

The webpage can be displayed over the internet: there is an endpoint I can see the page on.

Artifacts to be provided

Resulting publicly accessible URL
Kubernetes Deployment, Service, Ingress YAMLs
nginx configuration.

Steps to accomplish the homework.

Step1. create a dockerfile that will pull nginx:latest.

create an index.html file.

Copy the content of the index.html to /usr/share/nginx/html

Step2. Build the docker image. `docker build -t <nameofimage> </path/to/Dockerfile>

step3. Push the docker image to dockerhub (username tawfikiddrisu) or gcr.io.

step4. create a two-node cluster in GCP console. 

This can be done through Cloudshell using this command 

$gcloud config set compute/zone <zoneofchoice> #this sets the zone to deploy GKE Cluster

$gcloud container clusters create <clustername> --num-nodes <number> --scopes cloud-platform # this will create cluster with the requested number of nodes.

step5. create a deployment file for GKE.

Step6. Create a service.yaml file to expose the application.

step7. create a Ingress.yaml file.


